<?php

error_reporting(0);
ini_set('display_errors', 0);

date_default_timezone_set('Australia/Queensland');

$db = null;
$displayName = "";
$numItemsCart = 0;

session_name("animalgo");
session_start();

$authUser = isset($_SESSION["authUser"])? $_SESSION["authUser"] : "";

if(isset($authChk) == true) {
  if($authUser) {
    openDB();
    $query = "SELECT * FROM user WHERE uid = '$authUser' LIMIT 1";
    $result = $db->query($query);
    $row = $result->fetch_assoc();
    $displayName = $row['uname'];
  } else {
    header("location: ../login/login.php");
  }
}

function openDB() {
  global $db;
  if(!is_resource($db)) {
    $db = new mysqli(
      "localhost",
      "root",
      "",
      "animalgo"
    );
    if ($db->connect_errno) {
      $msg = "Failed to connect to MySQL: (" .
        $db->connect_errno . ") " .
        $db->connect_error;
        echo $msg;
    }
  }
}

function closeDB() {
  global $db;
  try {
    if(is_resource($db)) {
      $db->close();
    }
  } catch (Exception $e)
  {
    file_log('Error closing database');
    throw new Exception( 'Error closing database', 0, $e);
  }
}

?>