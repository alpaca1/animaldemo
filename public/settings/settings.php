<!-- Header -->
<?php include_once '../includes/header.php'; ?>
<!-- posting database values -->
<?php

  $uid = isset($_SESSION['authUser'])? $uid = $_SESSION['authUser'] :"";

  if(isset($_POST['update'])) {
    $chkLogin = false;
    // User table attributes
    $name = isset($_POST['name'])? $firstname = $_POST['name'] :"";
    $username = isset($_POST['username'])? $_POST['username'] : "";
    $password = isset($_POST['password'])? $_POST['password'] : "";
    $dob = isset($_POST['birth'])? $_POST['birth'] : "";
    $gender = isset($_POST['gender'])? $_POST['gender'] :"";
    $country = isset($_POST['country'])? $_POST['country'] : "";
    $language = isset($_POST['language'])? $_POST['language'] : "";
    $encry_pass = md5($password);

    // query
    openDB();
    $queryUpdate =
    "
      UPDATE `user` 
      SET 
        `uname`= '$name',
        `upassword`= '$encry_pass',
        `birth`= '$dob',
        `ugender`= $gender,
        `ucountry`= $country,
        `ulanguage`= $language
      WHERE 
        `uid` = $uid
    ";

    echo $queryUpdate;
    $result = $db->query($queryUpdate);
    if($db->error) {
      $msg = "Errormessage: %s\n" . $db->error;
      $chkLogin = false;
    } else {
      $chkLogin = true;
    }

    if($chkLogin == false) {
      $msg .= " Please try again.";
    } else {
      $msg = "User has been updated!";
    }

    $castingChk = $chkLogin ? 'true' : 'false';
    echo '<script type="text/javascript"> var msg = "'.$msg.'";</script>';
    echo '<script type="text/javascript"> var action = "'.$castingChk.'";</script>';

    closeDB();
  }
  
  openDB();
  $queryDetails = "SELECT `uname`, `upassword`, `birth`, `ugender`, `ucountry`, `ulanguage`, `picture` FROM `user` WHERE `uid` = $uid";
  $resultDetails = $db->query($queryDetails);
  $userData = $resultDetails->fetch_assoc();
  $queryGender ="SELECT gid, gname FROM  gender ";
  $resultGender = $db->query($queryGender);
  $queryCountry ="SELECT cid, cname FROM  countries ";
  $resultCountry = $db->query($queryCountry);
  $queryLanguage ="SELECT lid, lname FROM  languages";
  $resultLanguage= $db->query($queryLanguage);
  closeDB();

  echo '<script type="text/javascript">';
  echo 'var gender = "'.$userData['ugender'].'";';
  echo 'var country = "'.$userData['ucountry'].'";';
  echo 'var language = "'.$userData['ulanguage'].'";';
  echo '</script>';
?>
<!-- View -->
<div class="settings-content">
    <button class="settings-panel back" id="back"><i class="fas fa-chevron-left"></i></button>
    <img class="settings_user_img" src="../../uploads/images/user.png" alt="Smiley face" height="115" width="115">
    <hr>
    <h1 class="settings-title">Register account</h1>
    <form class="form-settings" method="post" action="<?PHP echo $_SERVER['PHP_SELF']; ?>">
        <input class="settings" type="text" name="name" id="name" placeholder="Full name" maxlength="50" value=<?php echo '"'.$userData['uname'].'"'; ?> >
        <input class="settings" type="password" name="password" id="password" placeholder="Password"  maxlength="10" required>
        <label for="gender">Select gender</label>
        <select class="settings" name="gender" id="gender" placeholder="Gender" style="padding: 8px 20px !important;border-radius: 30px !important;" required>
            <?php
                while ($row = $resultGender->fetch_assoc()) {
                    echo '<option value="'.$row['gid'].'">'.$row['gname'].'</option>';
                }
            ?>
        </select>
        <label for="gender">Select country</label>
        <select class="settings" name="country" id="country" style="padding: 8px 20px !important;border-radius: 30px !important;" required>
            <?php
                while ($row = $resultCountry->fetch_assoc()) {
                    echo '<option value="'.$row['cid'].'">'.$row['cname'].'</option>';
                }
            ?>
        </select>
        <input class="settings" type="date" name="birth" id="birth" placeholder="Date of birth" value=<?php echo '"'.$userData['birth'].'"'; ?> required>
        <label for="gender">Select language</label>
        <select class="settings" name="language" id="language" style="padding: 8px 20px !important;border-radius: 30px !important;" required>
            <?php
                while ($row = $resultLanguage->fetch_assoc()) {
                    echo '<option value="'.$row['lid'].'">'.$row['lname'].'</option>';
                }
            ?>
        </select>
        <input class="hidden" type="checkbox" id="update" name="update" value="update" checked>
        <button class="settings submit" type="submit">Save</button>
    </form>
</div>
<!-- toast -->
<?php include_once '../includes/toast.html'; ?>
<!-- View controller -->
<script type="text/javascript" src="settingsCtrl.js"></script>