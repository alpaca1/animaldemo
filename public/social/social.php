<!-- Header -->
<?php include_once '../includes/header.php'; ?>
<!-- Navegation bar -->
<?php include_once '../includes/feedNav.php'; ?>
<!-- posting database values -->
<?php
    $uid = isset($_SESSION['authUser'])? $uid = $_SESSION['authUser'] :"";

    openDB();

    $querySelectUserRequest =
    "
        SELECT 
            user.uid, 
            uname, 
            cname,
            path
        FROM 
            `user`,
            `user_picture`,
            `countries`
        WHERE
            picture = pid
        AND
            ucountry = cid
        AND 
            user.uid <> $uid
        AND
            user.uid IN 
            (
                SELECT friendid
            FROM 
                friends
            where
                friends.uid = $uid   
            and 
                status = 'request'        
            )   
    ";

    $resultSelectUserRequest = $db->query($querySelectUserRequest);

    $querySelectRequest =
    "
        SELECT 
            user.uid, 
            uname, 
            cname,
            path
        FROM 
            `user`,
            `user_picture`,
            `countries`
        WHERE
            picture = pid
        AND
            ucountry = cid
        AND 
            user.uid IN  
            (
                SELECT friends.uid FROM user
                INNER JOIN friends
                ON user.uid = friendid
                WHERE 
                    status = 'request'
                AND
                    user.uid = $uid
            )
    ";

    $resultSelectRequest = $db->query($querySelectRequest);
    
    $querySelectFriends =
    "
        SELECT 
            user.uid, 
            uname, 
            cname,
            path
        FROM 
            `user`,
            `user_picture`,
            `countries`
        WHERE
            picture = pid
        AND
            ucountry = cid
        AND 
            user.uid <> $uid
        AND 
        (
            user.uid IN 
            (
                SELECT friendid
            FROM 
                friends
            where
                friends.uid = $uid
            and 
                status = 'accepted'
            )
            OR
            user.uid IN 
            (
                SELECT friends.uid 
            FROM 
                friends
            where
                friendid = $uid
            and 
                status = 'accepted'
            )
        )
    ";

    $resultSelectFriends = $db->query($querySelectFriends);

    $querySelect =
    "
        SELECT 
            user.uid, 
            uname, 
            cname,
            path
        FROM 
            `user`,
            `user_picture`,
            `countries`
        WHERE
            picture = pid
        AND
            ucountry = cid
        AND 
            user.uid <> $uid
        AND
        (
            user.uid NOT IN 
            (
                SELECT friendid
            FROM 
                friends
            where
                friends.uid = $uid           
            )
            AND
            user.uid NOT IN 
            (
                SELECT friends.uid 
            FROM 
                friends
            where
                friendid = $uid
            )  
        )          
    ";

    $resultSelect = $db->query($querySelect);

    closeDB();
?>
<!-- View -->
<div class="feed-panel">
    <div class="social-head-button">
        <button id="friends">Friends</button>
        <button id="add">Add friends</button>
    </div>
    <div id="friend-panel">
        <h3 class="social-title-list"> My friends </h3>
        <?php
            while ($row = $resultSelectUserRequest->fetch_assoc()) {
            echo '<div class="user-item" style="background: #dee2e6;">';
            echo '<img class="settings_user_img" src="'.$row['path'].'" alt="Smiley face" height="60" width="60">';
            echo '<div>';
            echo '<p>'.$row['uname'].'</p>';
            echo '<p> Request pending </p>';
            echo '</div>';
            echo '<div>';
            echo '<button class="add-user-btn" onclick="removeFriend('.$row['uid'].')" >Unsend</button>';
            echo '</div>';
            echo '</div>';
            }
        ?>
        <?php
            while ($row = $resultSelectRequest->fetch_assoc()) {
            echo '<div class="user-item" style="background: #dee2e6;">';
            echo '<img class="settings_user_img" src="'.$row['path'].'" alt="Smiley face" height="60" width="60">';
            echo '<div>';
            echo '<p>'.$row['uname'].'</p>';
            echo '<p>'.$row['cname'].'</p>';
            echo '</div>';
            echo '<div>';
            echo '<button class="add-user-btn" onclick="acceptFriend('.$row['uid'].')" >Accept</button>';
            echo '</div>';
            echo '</div>';
            }
        ?>
        <?php
            while ($row = $resultSelectFriends->fetch_assoc()) {
            echo '<div class="user-item">';
            echo '<img class="settings_user_img" src="'.$row['path'].'" alt="Smiley face" height="60" width="60">';
            echo '<div>';
            echo '<p>'.$row['uname'].'</p>';
            echo '<p>'.$row['cname'].'</p>';
            echo '</div>';
            echo '<div>';
            echo '<button class="rm-user-btn" onclick="removeFriend('.$row['uid'].')" ><i class="fas fa-user-slash"></i></button>';
            echo '</div>';
            echo '</div>';
            }
        ?>
    </div>
    <div id="add-panel">
        <h3 class="social-title-list"> Add a new friend </h3>
        <?php
            while ($row = $resultSelect->fetch_assoc()) {
            echo '<div class="user-item">';
            echo '<img class="settings_user_img" src="'.$row['path'].'" alt="Smiley face" height="60" width="60">';
            echo '<div>';
            echo '<p>'.$row['uname'].'</p>';
            echo '<p>'.$row['cname'].'</p>';
            echo '</div>';
            echo '<div>';
            echo '<button class="add-user-btn" onclick="addFriend('.$row['uid'].')" >Add</button>';
            echo '</div>';
            echo '</div>';
            }
        ?>
    </div>
</div>
<!-- toast -->
<?php include_once '../includes/toast.html'; ?>
<!-- View controller -->
<script type="text/javascript" src="socialCtrl.js"></script>
<!-- Footer -->
<?php include_once '../includes/footer.php'; ?>