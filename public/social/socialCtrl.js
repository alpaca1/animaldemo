// Erase background image
document.body.style.backgroundImage = "none";
document.getElementById("body").style.backgroundImage = "none";
$('#nav-title').html('App users');

$(back).click(function(){
    window.location.href = "../feed/feed.php";
});

$("#add-panel").hide();
$("#friends").css("background","#a0e679");
$("#friends").css("border","2px solid #66944d");
$("#friends").css("box-shadow","1px 4px 5px -1px rgba(150,150,150,1)");
$("#friends").css("color","#155724");

$(".close").click(function(){
    location.reload();
});

$("#friends").click(function(){
    $("#friend-panel").show();
    $("#add-panel").hide();
    $("#friends").css("background","#a0e679");
    $("#friends").css("border","2px solid #66944d");
    $("#friends").css("box-shadow","1px 4px 5px -1px rgba(150,150,150,1)");
    $("#friends").css("color","#155724");
    $("#add").css("background","#e9ecef");
    $("#add").css("border","1px solid #c3c3c3");
    $("#add").css("box-shadow","none");
    $("#add").css("color","#8e8e8e");
});

$("#add").click(function(){
    $("#friend-panel").hide();
    $("#add-panel").show();
    $("#friends").css("background","#e9ecef");
    $("#friends").css("border","1px solid #c3c3c3");
    $("#friends").css("box-shadow","none");
    $("#friends").css("color","#8e8e8e");
    $("#add").css("background","#a0e679");
    $("#add").css("border","2px solid #66944d");
    $("#add").css("box-shadow","1px 4px 5px -1px rgba(150,150,150,1)");
    $("#add").css("color","#155724");
});

function addFriend(friend){
    request = $.ajax({
        url: "../../server/daos/friends.php",
        type: "post",
        data: { 
            add : friend
        }
    });
    
    request.done(function (response, textStatus, jqXHR){
        $("#toast").toast({ delay: 1000 });
        $("#toast").toast("show");
        $(".toast-body").html("Your friend request has been sent.");
        setTimeout(function(action){
            location.reload();
        }, 1500);
        
    });
}

function removeFriend(friend){
    request = $.ajax({
        url: "../../server/daos/friends.php",
        type: "post",
        data: { 
            remove : friend
        }
    });
    
    request.done(function (response, textStatus, jqXHR){
        $("#toast").toast({ delay: 1000 });
        $("#toast").toast("show");
        $(".toast-body").html("Friend has been deleted.");
        setTimeout(function(action){
            location.reload();
        }, 1500);
        
    });
}

function acceptFriend(friend){
    request = $.ajax({
        url: "../../server/daos/friends.php",
        type: "post",
        data: { 
            accept : friend
        }
    });
    
    request.done(function (response, textStatus, jqXHR){
        $("#toast").toast({ delay: 1500 });
        $("#toast").toast("show");
        $(".toast-body").html("Your friend request has been sent.");
        setTimeout(function(action){
            location.reload();
        }, 1000);
        
    });
}

