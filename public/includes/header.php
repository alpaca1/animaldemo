<!DOCTYPE html>
<html lang="en">
<!doctype html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Include Styles -->
    <?php include_once '../../server/includes/style.html'; ?>
    <!-- Include Javascript -->
    <?php include_once '../../server/includes/javascript.html'; ?>
  </head>
  <body>
    <!-- Include server configuration -->
    <?php include_once '../../server/config/server.php'; ?>
  <div id='body'>