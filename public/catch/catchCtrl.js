$(back).click(function(){
    window.location.href = "../map/map.php";
});

var actual_JSON;
var url_string = window.location.href;
var url = new URL(url_string);
var id = url.searchParams.get("id");
var name = url.searchParams.get("name");
var commonName = url.searchParams.get("cname");
var img = url.searchParams.get("img");
var coor = url.searchParams.get("coor");
coordinate = coor.split(",");
console.log(id+coordinate[0]+coordinate[1]);

var modelRender;
commonName = commonName.toLowerCase();
if (commonName.includes("dingo") || commonName.includes("dog") || commonName.includes("fox")) {
    modelRender = '../../uploads/models/dingo.gltf';
} else if (commonName.includes("dolphin")) {
    modelRender = '../../uploads/models/dolphin.gltf';
} else if (commonName.includes("koala")) {
    modelRender = '../../uploads/models/koala.gltf';
} else if (commonName.includes("possum") || commonName.includes("rat")) {
    modelRender = '../../uploads/models/possum.gltf';
} else if (commonName.includes("glider") || commonName.includes("squirrel")) {
    modelRender = '../../uploads/models/squirrel.gltf';
} else if (commonName.includes("rabbit") || commonName.includes("hare")) {
    modelRender = '../../uploads/models/hare.gltf';
} else if (commonName.includes("deer") || commonName.includes("horse") || commonName.includes("donkey")) {
    modelRender = '../../uploads/models/deer.gltf';
} else {
    modelRender = '../../uploads/models/kangaroo.gltf';
}

if (img == "") {
    img = "../../uploads/images/857.png";
}

$(".close").click(function(){
    window.location.href = "../map/map.php";
});

$("#catch").click(function (){
    // Ajax call to capture an animal
    request = $.ajax({
        url: "../../server/daos/album.php",
        type: "post",
        data: { 
            id : id,
            name : name,
            path : img
        }
    });
    // Callback handler on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Animal saved");
        $("#toast").toast({ delay: 100 });
        $("#toast").toast("show");
        $(".toast-body").html("Excellent! Animal catched!!");
        setTimeout(function(action){
                window.location.href = "../map/map.php";
        }, 1000);
    });
    // Callback handler on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
            "error message: "+
            textStatus, errorThrown
        );
    });
});


mapboxgl.accessToken = 'pk.eyJ1IjoiamFpbWUxNjkyIiwiYSI6ImNrMG01NjRtNDEzODYzbm12ZWd4Zm82NHgifQ.4bbxwtQ5W7WIRyE6oZHJ7A';

var map = window.map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/streets-v11',
zoom: 15,
center: [coordinate[0],coordinate[1]],
pitch: 60,
antialias: true
});

var modelOrigin = [coordinate[0],coordinate[1]];
var modelAltitude = 0;
var modelRotate = [Math.PI / 2, 0, 0];
var modelAsMercatorCoordinate = mapboxgl.MercatorCoordinate.fromLngLat(modelOrigin, modelAltitude);

// transformation parameters to position, rotate and scale the 3D model onto the map
var modelTransform = {
    translateX: modelAsMercatorCoordinate.x,
    translateY: modelAsMercatorCoordinate.y,
    translateZ: modelAsMercatorCoordinate.z,
    rotateX: modelRotate[0],
    rotateY: modelRotate[1],
    rotateZ: modelRotate[2],
    scale: modelAsMercatorCoordinate.meterInMercatorCoordinateUnits()
};

var THREE = window.THREE;

// configuration of the custom layer for a 3D model per the CustomLayerInterface

var customLayer = {
    id: '3d-model',
    type: 'custom',
    renderingMode: '3d',
    onAdd: function(map, gl) {
    this.camera = new THREE.Camera();
    this.scene = new THREE.Scene();
    
    // create two three.js lights to illuminate the model
    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(0, -70, 100).normalize();
    this.scene.add(directionalLight);

    var directionalLight2 = new THREE.DirectionalLight(0xffffff);
    directionalLight2.position.set(0, 70, 100).normalize();
    this.scene.add(directionalLight2);

    // use the three.js GLTF loader to add the 3D model to the three.js scene
    var loader = new THREE.GLTFLoader();
    loader.load(modelRender, (function (gltf) {
    this.scene.add(gltf.scene);
    }).bind(this));
    this.map = map;

    // use the Mapbox GL JS map canvas for three.js
    this.renderer = new THREE.WebGLRenderer({
    canvas: map.getCanvas(),
    context: gl,
    antialias: true
    });
    
    this.renderer.autoClear = false;
    },
    render: function(gl, matrix) {
    var rotationX = new THREE.Matrix4().makeRotationAxis(new THREE.Vector3(1, 0, 0), modelTransform.rotateX);
    var rotationY = new THREE.Matrix4().makeRotationAxis(new THREE.Vector3(0, 1, 0), modelTransform.rotateY);
    var rotationZ = new THREE.Matrix4().makeRotationAxis(new THREE.Vector3(0, 0, 1), modelTransform.rotateZ);
    
    var m = new THREE.Matrix4().fromArray(matrix);
    var l = new THREE.Matrix4().makeTranslation(modelTransform.translateX, modelTransform.translateY, modelTransform.translateZ)
    .scale(new THREE.Vector3(modelTransform.scale, -modelTransform.scale, modelTransform.scale))
    .multiply(rotationX)
    .multiply(rotationY)
    .multiply(rotationZ);
    
    this.camera.projectionMatrix.elements = matrix;
    this.camera.projectionMatrix = m.multiply(l);
    this.renderer.state.reset();
    this.renderer.render(this.scene, this.camera);
    this.map.triggerRepaint();
    }
};

map.on('style.load', function() {
    map.addLayer(customLayer, 'waterway-label');
});